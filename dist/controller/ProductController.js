"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductController = void 0;
const BaseController_1 = require("./base/BaseController");
const Product_1 = __importDefault(require("../schema/Product"));
const ProductEavAttributeValue_1 = __importDefault(require("../schema/ProductEavAttributeValue"));
class ProductController extends BaseController_1.BaseController {
    constructor(request) {
        super();
        this.product = new Product_1.default();
        this.id = "";
        this.request = request;
        this.productEavAttributeValue = new ProductEavAttributeValue_1.default();
    }
    setProduct(product) {
        this.product = product;
        return this;
    }
    getProduct() {
        return this.product;
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
        return this;
    }
    create() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.product = this.request.body;
                var product = yield Product_1.default.create(this.product);
                return product;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                var product = yield Product_1.default.findOneAndUpdate({ _id: this.id }, params, { new: true });
                return product;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var product = yield Product_1.default.deleteOne({ _id: this.id });
                return product;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var product = yield Product_1.default.findOne({ _id: this.id });
                return product;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    gets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var Products = yield Product_1.default.find().populate("product_type_id", ["name"], "ProductType");
                return Products;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.ProductController = ProductController;
//# sourceMappingURL=ProductController.js.map