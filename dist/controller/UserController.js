"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserController = void 0;
const BaseController_1 = require("./base/BaseController");
const User_1 = __importDefault(require("../schema/User"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
class UserController extends BaseController_1.BaseController {
    constructor(request) {
        super();
        this.user = new User_1.default();
        this.id = "";
        this.request = request;
    }
    setUser(user) {
        this.user = user;
        return this;
    }
    getUser() {
        return this.user;
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
        return this;
    }
    hashPwd(password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return bcryptjs_1.default.hash(password, bcryptjs_1.default.genSaltSync(10));
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    create() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                params.password = yield this.hashPwd(params.password);
                var user = yield User_1.default.create(params);
                return user;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                params.password = yield this.hashPwd(params.password);
                var user = yield User_1.default.findOneAndUpdate({ _id: this.id }, params, { new: true });
                return user;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var user = yield User_1.default.deleteOne({ _id: this.id });
                return user;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var user = yield User_1.default.findOne({ _id: this.id });
                return user;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    gets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var users = yield User_1.default.find();
                return users;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.UserController = UserController;
//# sourceMappingURL=UserController.js.map