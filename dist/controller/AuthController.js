"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const BaseController_1 = require("./base/BaseController");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const GenerateToken_1 = require("../helper/GenerateToken");
const Auth_1 = require("../helper/Auth");
const Mailer_1 = require("../helper/Mailer");
const User_1 = __importDefault(require("../schema/User"));
const index_1 = require("../config/index");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
class AuthController extends BaseController_1.BaseController {
    constructor(request) {
        super();
        this.authInfo = {
            username: "default",
            password: "default"
        };
        this.request = request;
    }
    login() {
        return __awaiter(this, void 0, void 0, function* () {
            var auth = new Auth_1.Auth(this.request);
            var user = yield auth.execute();
            if (user) {
                const token = jsonwebtoken_1.default.sign({
                    _id: user._id,
                    email: user.email,
                    phone: user.phone,
                    role: user.role,
                    firstname: user.firstname,
                    lastname: user.lastname,
                    image: user.image
                }, index_1.TOKEN_SECRET);
                return { "token": token, "user": user };
            }
            else {
                return null;
            }
        });
    }
    hashPwd(password) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                return bcryptjs_1.default.hash(password, bcryptjs_1.default.genSaltSync(10));
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    forgetPassword() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var email = this.request.body.email;
                var user = yield User_1.default.findOne({ email: email });
                if (user) {
                    var token = new GenerateToken_1.GenerateToken(6).execute();
                    var mailer = yield new Mailer_1.Mailer({ "to": email, "html": "please go to web and enter this token to reset password: <strong>" + token + "</strong>", "subject": "Bida Reset Password" }).execute();
                    yield User_1.default.findOneAndUpdate({ email: email }, { "token": token });
                    return true;
                }
                else {
                    return false;
                }
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    resetPassword() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var resetInfo = this.request.body;
                var newPass = yield this.hashPwd(resetInfo.password);
                var user = yield User_1.default.findOneAndUpdate({ email: resetInfo.email, token: resetInfo.token }, { password: newPass, token: "" }, { new: true });
                return user;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.AuthController = AuthController;
//# sourceMappingURL=AuthController.js.map