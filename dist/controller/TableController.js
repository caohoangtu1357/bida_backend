"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TableController = void 0;
const BaseController_1 = require("./base/BaseController");
const Table_1 = __importDefault(require("../schema/Table"));
class TableController extends BaseController_1.BaseController {
    constructor(request) {
        super();
        this.table = new Table_1.default();
        this.id = "";
        this.request = request;
    }
    setTable(table) {
        this.table = table;
        return this;
    }
    getTable() {
        return this.table;
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
        return this;
    }
    create() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                var table = yield Table_1.default.create(params);
                return table;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // const params: ITable = this.request.body;
                var tableOld = yield Table_1.default.findOne({ _id: this.id });
                var newTable = new Table_1.default();
                if (tableOld != null) {
                    if (tableOld.status == "empty") {
                        newTable = yield Table_1.default.findOneAndUpdate({ _id: this.id }, { status: "not-empty" }, { new: true });
                    }
                    else {
                        newTable = yield Table_1.default.findOneAndUpdate({ _id: this.id }, { status: "empty" }, { new: true });
                    }
                }
                return newTable;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var table = yield Table_1.default.deleteOne({ _id: this.id });
                return table;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var table = yield Table_1.default.findOne({ _id: this.id });
                return table;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    gets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var tables = yield Table_1.default.find().populate('table_type_id', ["name"], "TableType");
                return tables;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.TableController = TableController;
//# sourceMappingURL=TableController.js.map