"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderController = void 0;
const BaseController_1 = require("./base/BaseController");
const Order_1 = __importDefault(require("../schema/Order"));
const mongoose_1 = __importDefault(require("mongoose"));
const OrderDetail_1 = __importDefault(require("../schema/OrderDetail"));
const TableController_1 = require("../controller/TableController");
class OrderController extends BaseController_1.BaseController {
    constructor(request) {
        super();
        this.order = new Order_1.default();
        this.orderDetail = new OrderDetail_1.default();
        this.orderDetails = {
            order_detail: [
                new OrderDetail_1.default()
            ]
        };
        this.bill = {
            order: new Order_1.default(),
            order_detail: [new OrderDetail_1.default()]
        };
        this.id = "";
        this.request = request;
    }
    setOrder(order) {
        this.order = order;
        return this;
    }
    getOrder() {
        return this.order;
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
        return this;
    }
    create() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                this.orderDetails = this.request.body;
                this.order = yield Order_1.default.create(params);
                let orderDetailCreated = [];
                for (let i = 0; i < this.orderDetails.order_detail.length; i++) {
                    this.orderDetail.requirement = this.orderDetails.order_detail[i].requirement;
                    this.orderDetail.product_id = this.orderDetails.order_detail[i].product_id;
                    this.orderDetail.order_id = this.order._id;
                    orderDetailCreated.push(yield OrderDetail_1.default.create(this.orderDetail));
                }
                this.bill.order = this.order;
                this.bill.order_detail = orderDetailCreated;
                return this.bill;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var params = this.request.body;
                var order = yield Order_1.default.findOneAndUpdate({ _id: this.id }, { status: "da xac nhan" }, { new: true });
                var table = new TableController_1.TableController(this.request);
                if (order != null) {
                    table.setId(order.table_id.toString());
                    yield table.update();
                }
                return order;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var order = yield Order_1.default.deleteOne({ _id: this.id });
                var orderDetail = yield OrderDetail_1.default.deleteMany({ order_id: mongoose_1.default.Types.ObjectId(this.id) });
                return order;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var order = yield Order_1.default.findOne({ _id: this.id });
                return order;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    gets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var Orders = yield Order_1.default.find()
                    .populate("user_id", ["firstname", "lastname"], "User")
                    .populate("table_id", ["name"], "Table");
                return Orders;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.OrderController = OrderController;
//# sourceMappingURL=OrderController.js.map