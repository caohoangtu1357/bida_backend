"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductTypeController = void 0;
const BaseController_1 = require("./base/BaseController");
const ProductType_1 = __importDefault(require("../schema/ProductType"));
class ProductTypeController extends BaseController_1.BaseController {
    constructor(request) {
        super();
        this.productType = new ProductType_1.default();
        this.id = "";
        this.request = request;
    }
    setProductType(productType) {
        this.productType = productType;
        return this;
    }
    getProductType() {
        return this.productType;
    }
    getId() {
        return this.id;
    }
    setId(id) {
        this.id = id;
        return this;
    }
    create() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                var productType = yield ProductType_1.default.create(params);
                return productType;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const params = this.request.body;
                var productType = yield ProductType_1.default.findOneAndUpdate({ _id: this.id }, params, { new: true });
                return productType;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    delete() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var productType = yield ProductType_1.default.deleteOne({ _id: this.id });
                return productType;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    get() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var productType = yield ProductType_1.default.findOne({ _id: this.id });
                return productType;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
    gets() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                var ProductTypes = yield ProductType_1.default.find();
                return ProductTypes;
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.ProductTypeController = ProductTypeController;
//# sourceMappingURL=ProductTypeController.js.map