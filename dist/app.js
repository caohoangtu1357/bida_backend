"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const morgan_1 = __importDefault(require("morgan"));
const index_1 = require("./routes/index");
const mongoose_1 = __importDefault(require("mongoose"));
const index_2 = require("./config/index");
const bodyParser = __importStar(require("body-parser"));
const cors_1 = __importDefault(require("cors"));
class App {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.connect_db();
        this.routes = new index_1.IndexRouter(this.app);
    }
    getApp() {
        return this.app;
    }
    config() {
        this.app.use(cors_1.default({ credentials: true, origin: true, exposedHeaders: ["auth-token"] }));
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.set('views', path_1.default.join(__dirname, 'views'));
        this.app.set('view engine', 'jade');
        this.app.use(morgan_1.default('dev'));
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(cookie_parser_1.default());
        console.log(path_1.default.join(__dirname, 'public'));
        this.app.use(express_1.default.static(path_1.default.join(__dirname, '../public')));
    }
    connect_db() {
        console.log(index_2.URL_DB);
        mongoose_1.default.connect(index_2.URL_DB, { useNewUrlParser: true, useUnifiedTopology: true }, err => {
            if (err) {
                console.log("connect fail");
                console.log(err);
            }
            else {
                console.log("connected to mongodb");
            }
        });
    }
}
exports.default = new App().getApp();
//# sourceMappingURL=app.js.map