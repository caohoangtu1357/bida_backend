"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyToken = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const index_1 = require("../config/index");
class VerifyToken {
    constructor(req, res, next) {
        this.req = req;
        this.res = res;
        this.next = next;
    }
    execute() {
        var token = this.req.header("auth-token");
        if (!token) {
            token = this.req.cookies["auth-cookie"];
        }
        if (!token) {
            return this.res
                .status(401)
                .send({ message: "Bạn không có quyền truy cập, vui lòng đăng nhập" });
        }
        try {
            var verified = jsonwebtoken_1.default.verify(token, index_1.TOKEN_SECRET);
            this.req.user = verified;
            return verified;
        }
        catch (error) {
            return this.res.status(400).send({ message: "Token không hợp lệ" });
        }
    }
}
exports.VerifyToken = VerifyToken;
//# sourceMappingURL=VerifyToken.js.map