"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Auth = void 0;
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const User_1 = __importDefault(require("../schema/User"));
class Auth {
    constructor(req) {
        this.req = req;
    }
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            var authInfo = this.req.body;
            const email = authInfo.username;
            const password = authInfo.password;
            var user = yield User_1.default.findOne({ $or: [{ email: email }, { phone: email }] });
            if (!user) {
                return null;
            }
            try {
                var isMatch = yield bcryptjs_1.default.compare(password, user.password);
                if (isMatch) {
                    return user;
                }
                else {
                    return null;
                }
            }
            catch (error) {
                throw new Error(error);
            }
        });
    }
}
exports.Auth = Auth;
//# sourceMappingURL=Auth.js.map