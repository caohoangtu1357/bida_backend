"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Uploader = void 0;
const multer_1 = __importDefault(require("multer"));
const path_1 = __importDefault(require("path"));
const util = require("util");
class Uploader {
    constructor(destinationPath, fileName, req, res, next) {
        this.destinationPath = "upload/image";
        this.imageName = "";
        this.destinationPath = destinationPath;
        this.fileName = fileName;
        this.req = req;
        this.res = res;
        this.next = next;
    }
    execute() {
        return __awaiter(this, void 0, void 0, function* () {
            var upload = yield util.promisify(this.config().single(this.fileName));
            yield upload(this.req, this.res);
            return this.destinationPath + "/" + this.req.file.filename;
        });
    }
    config() {
        var self = this;
        var storage = multer_1.default.diskStorage({
            destination: function (req, file, cb) {
                return __awaiter(this, void 0, void 0, function* () {
                    cb(null, path_1.default.join(__dirname, "../../public/" + self.destinationPath));
                });
            },
            filename: function (req, file, cb) {
                return __awaiter(this, void 0, void 0, function* () {
                    const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
                    var imageName = uniqueSuffix + "-" + file.originalname;
                    var reg = new RegExp("[ ]+", "g");
                    imageName = imageName.replace(reg, "");
                    cb(null, imageName);
                });
            },
        });
        return multer_1.default({ storage: storage });
    }
}
exports.Uploader = Uploader;
//# sourceMappingURL=FileUploader.js.map