"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GenerateToken = void 0;
class GenerateToken {
    constructor(tokenLength) {
        this.tokenLength = tokenLength;
    }
    makeId(tokenLength) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < tokenLength; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    setTokenLength(tokenLength) {
        this.tokenLength = tokenLength;
    }
    execute() {
        return this.makeId(this.tokenLength);
    }
}
exports.GenerateToken = GenerateToken;
//# sourceMappingURL=GenerateToken.js.map