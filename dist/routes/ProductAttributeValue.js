"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.productAttributeValue = void 0;
const BaseRoute_1 = require("./base/BaseRoute");
const ProductAttributeValueController_1 = require("../controller/ProductAttributeValueController");
class productAttributeValue extends BaseRoute_1.BaseRoute {
    constructor(app) {
        super(app);
        this.prefix = "/product-attribute-value";
    }
    routes() {
        this.create();
        this.update();
        this.delete();
        this.get();
        this.gets();
    }
    create() {
        this.app.route(this.prefix + "/create").post((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let productController = new ProductAttributeValueController_1.ProductAttributeValueValueController(req);
            try {
                let user = yield productController.create();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    update() {
        this.app.route(this.prefix + "/update/:id").put((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let productController = new ProductAttributeValueController_1.ProductAttributeValueValueController(req);
            try {
                productController.setId(req.param("id"));
                let user = yield productController.update();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    get() {
        this.app.route(this.prefix + "/get/:id").get((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let productController = new ProductAttributeValueController_1.ProductAttributeValueValueController(req);
            try {
                productController.setId(req.param("id"));
                let user = yield productController.get();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    gets() {
        this.app.route(this.prefix + "/gets").get((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let productController = new ProductAttributeValueController_1.ProductAttributeValueValueController(req);
            try {
                let user = yield productController.gets();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    delete() {
        this.app.route(this.prefix + "/delete/:id").delete((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let productController = new ProductAttributeValueController_1.ProductAttributeValueValueController(req);
            try {
                productController.setId(req.param("id"));
                let user = yield productController.delete();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
}
exports.productAttributeValue = productAttributeValue;
//# sourceMappingURL=ProductAttributeValue.js.map