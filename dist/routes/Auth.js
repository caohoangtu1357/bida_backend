"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Auth = void 0;
const BaseRoute_1 = require("./base/BaseRoute");
const AuthController_1 = require("../controller/AuthController");
class Auth extends BaseRoute_1.BaseRoute {
    constructor(app) {
        super(app);
    }
    routes() {
        this.login();
        this.forgetPassword();
        this.resetPassword();
        this.isLogged();
    }
    login() {
        this.app.route("/login").post((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let authController = new AuthController_1.AuthController(req);
            try {
                let auth = yield authController.login();
                if (auth === null || auth === void 0 ? void 0 : auth.user) {
                    res.status(200).header("auth-token", auth === null || auth === void 0 ? void 0 : auth.token).send(auth === null || auth === void 0 ? void 0 : auth.user);
                }
                else {
                    res.status(401).header("auth-token", auth === null || auth === void 0 ? void 0 : auth.token).send({ "message": "Invalid credential" });
                }
            }
            catch (error) {
                console.log(error);
                res.status(500).send(error.message);
            }
        }));
    }
    forgetPassword() {
        this.app.route("/forget-password").post((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let authController = new AuthController_1.AuthController(req);
            try {
                let token = yield authController.forgetPassword();
                if (token) {
                    res.status(200).send({ "status": "send token to mail success" });
                }
                else {
                    res.status(500).send({ "status": "user not found" });
                }
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    resetPassword() {
        this.app.route("/reset-password").post((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let authController = new AuthController_1.AuthController(req);
            try {
                let user = yield authController.resetPassword();
                if (user) {
                    res.status(200).send({ status: "success", "user": user, "message": "password changed" });
                }
                else {
                    res.status(500).send({ status: "failure", "user": user, "message": "invalid token" });
                }
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    isLogged() {
        this.app.route("/is-logged").get((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            var user = this.verifyUserLogged(req, res, next);
            try {
                if (user) {
                    res.status(200).send(user);
                }
                else {
                    res.status(401).json({ "message": "User not logged" });
                }
            }
            catch (error) {
                res.status(500).json({ "message": "Server go down" });
            }
        }));
    }
}
exports.Auth = Auth;
//# sourceMappingURL=Auth.js.map