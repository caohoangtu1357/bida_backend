"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Image = void 0;
const BaseRoute_1 = require("./base/BaseRoute");
const FileUploader_1 = require("../helper/FileUploader");
class Image extends BaseRoute_1.BaseRoute {
    constructor(app) {
        super(app);
        this.prefix = "/image";
    }
    routes() {
        this.create();
    }
    create() {
        this.app.route(this.prefix + "/upload").post((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                var imageName = yield new FileUploader_1.Uploader("upload/image", "image", req, res, next).execute();
                res.status(200).send({ uploaded: imageName });
            }
            catch (error) {
                console.log(error);
                res.status(500).send(error.message);
            }
        }));
    }
}
exports.Image = Image;
//# sourceMappingURL=Image.js.map