"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseRoute = void 0;
const VerifyToken_1 = require("../../middleware/VerifyToken");
class BaseRoute {
    constructor(app) {
        this.app = app;
    }
    routes() {
    }
    verifyUserLogged(req, res, next) {
        var middleWare = new VerifyToken_1.VerifyToken(req, res, next);
        return middleWare.execute();
    }
}
exports.BaseRoute = BaseRoute;
//# sourceMappingURL=BaseRoute.js.map