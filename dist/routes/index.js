"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IndexRouter = void 0;
const BaseRoute_1 = require("./base/BaseRoute");
const User_1 = require("./User");
const Table_1 = require("./Table");
const Auth_1 = require("./Auth");
const Order_1 = require("./Order");
const Product_1 = require("./Product");
const ProductType_1 = require("./ProductType");
const TableType_1 = require("./TableType");
const Image_1 = require("./Image");
const ProductAttribute_1 = require("./ProductAttribute");
const ProductAttributeValue_1 = require("./ProductAttributeValue");
const path_1 = __importDefault(require("path"));
class IndexRouter extends BaseRoute_1.BaseRoute {
    constructor(app) {
        super(app);
        this.regisNewRoute(new User_1.User(app));
        this.regisNewRoute(new Table_1.Table(app));
        this.regisNewRoute(new Auth_1.Auth(app));
        this.regisNewRoute(new Order_1.Order(app));
        this.regisNewRoute(new Product_1.Product(app));
        this.regisNewRoute(new ProductType_1.ProductType(app));
        this.regisNewRoute(new TableType_1.TableType(app));
        this.regisNewRoute(new Image_1.Image(app));
        this.regisNewRoute(new ProductAttribute_1.ProductAttribute(app));
        this.regisNewRoute(new ProductAttributeValue_1.productAttributeValue(app));
        this.app.route("/").get((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            res.sendFile(path_1.default.join(__dirname + '../../../public/index.html'));
        }));
        //regis new route
    }
    regisNewRoute(routeClass) {
        routeClass.routes();
    }
}
exports.IndexRouter = IndexRouter;
//# sourceMappingURL=index.js.map