"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Table = void 0;
const BaseRoute_1 = require("./base/BaseRoute");
const TableController_1 = require("../controller/TableController");
class Table extends BaseRoute_1.BaseRoute {
    constructor(app) {
        super(app);
        this.prefix = "/table";
    }
    routes() {
        this.create();
        this.update();
        this.delete();
        this.get();
        this.gets();
    }
    create() {
        this.app.route(this.prefix + "/create").post((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let tableController = new TableController_1.TableController(req);
            try {
                console.log(req.body);
                let user = yield tableController.create();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    update() {
        this.app.route(this.prefix + "/update/:id").put((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let tableController = new TableController_1.TableController(req);
            try {
                tableController.setId(req.param("id"));
                let user = yield tableController.update();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    get() {
        this.app.route(this.prefix + "/get/:id").get((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let tableController = new TableController_1.TableController(req);
            try {
                tableController.setId(req.param("id"));
                let user = yield tableController.get();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    gets() {
        this.app.route(this.prefix + "/gets").get((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let tableController = new TableController_1.TableController(req);
            try {
                tableController.setId(req.param("id"));
                let user = yield tableController.gets();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
    delete() {
        this.app.route(this.prefix + "/delete/:id").delete((req, res, next) => __awaiter(this, void 0, void 0, function* () {
            let tableController = new TableController_1.TableController(req);
            try {
                tableController.setId(req.param("id"));
                let user = yield tableController.delete();
                res.status(200).send(user);
            }
            catch (error) {
                res.status(500).send(error.message);
            }
        }));
    }
}
exports.Table = Table;
//# sourceMappingURL=Table.js.map