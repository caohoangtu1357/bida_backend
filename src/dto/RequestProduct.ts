import {IProduct} from '../schema/Product';
import { IProductEavAttributeValue } from '../schema/ProductEavAttributeValue';

export interface IRequestProduct{
    product:IProduct;
    product_attribute_values:[IProductEavAttributeValue]
}