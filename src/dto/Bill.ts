import { IOrderDetail } from '../schema/OrderDetail';
import { IOrder } from '../schema/Order';

export interface IBill{
    order_detail: IOrderDetail[];
    order:IOrder;
}