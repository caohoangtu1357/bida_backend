import {Schema} from "mongoose"

export interface IRequestAttributeValues{
    attribute_values:[
        {
            value:string,
            product_attribute_id:Schema.Types.ObjectId
        }
    ];
    
}