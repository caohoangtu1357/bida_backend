import {BaseController} from './base/BaseController';
import Product,{IProduct} from '../schema/Product';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import { promises } from 'fs';
import bcryptjs from "bcryptjs";
import Schema from 'mongoose';
import ProductEavAttributeValue,{ IProductEavAttributeValue } from '../schema/ProductEavAttributeValue';
import { IRequestProduct } from '../dto/RequestProduct';
import {IRequestAttributeValues} from "../dto/RequestAttributeValues";

export class ProductController extends BaseController implements IBaseController{

    private product:IProduct;

    private id:string;

    private request:Request;



    private productEavAttributeValue:IProductEavAttributeValue;

    public constructor(request:Request){
        super();
        this.product=new Product();
        this.id="";
        this.request = request;
        this.productEavAttributeValue = new ProductEavAttributeValue();
    
    }


    public setProduct(product:IProduct):this{
        this.product = product;
        return this;
    }

    public getProduct():IProduct{
        return this.product;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<IProduct>{
        try {
            this.product = this.request.body;

            var product:IProduct = await Product.create<IProduct>(this.product);
            
            return product;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(IProduct|null)>{
        try {
            const params: IProduct = this.request.body;
            var product:(IProduct|null) = await Product.findOneAndUpdate({_id:this.id},params,{new:true});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var product = await Product.deleteOne({_id:this.id});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(IProduct|null)> {
        try {
            var product:(IProduct|null) = await Product.findOne({_id:this.id});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var Products = await Product.find().populate("product_type_id",["name"],"ProductType");
            return Products;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}