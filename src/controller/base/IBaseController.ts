import {BaseController} from "./BaseController";

export interface IBaseController{
    create():any;
    update():any;
    delete():any;
    get():any;
    gets():any;
}