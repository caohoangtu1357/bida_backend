import {BaseController} from './base/BaseController';
import User,{IUser} from '../schema/User';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import { promises } from 'fs';
import bcryptjs from "bcryptjs";
import {Schema} from 'mongoose';
import {ObjectID} from "mongodb";


export class UserController extends BaseController implements IBaseController{

    private user:IUser;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.user= new User();
        this.id="";
        this.request = request;
        
    }


    public setUser(user:IUser):this{
        this.user = user;
        return this;
    }

    public getUser():IUser{
        return this.user;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }

    public async hashPwd(password:string):Promise<string>{
        try {
            return bcryptjs.hash(password,bcryptjs.genSaltSync(10));
        } catch (error) {
            throw new Error(error);
        }
    }


    public async create():Promise<IUser>{
        try {
            const params: IUser = this.request.body;
            
            params.password =await this.hashPwd(params.password);

            var user:IUser = await User.create<IUser>(params);
            return user;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(IUser|null)>{
        try {
            const params: IUser = this.request.body;
            params.password =await this.hashPwd(params.password);
            var user:(IUser|null) = await User.findOneAndUpdate({_id:this.id},params,{new:true});
            return user;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var user = await User.deleteOne({_id:this.id});
            return user;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(IUser|null)> {
        try {
            var user:(IUser|null) = await User.findOne({_id:this.id});
            return user;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var users = await User.find();
            return users;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}