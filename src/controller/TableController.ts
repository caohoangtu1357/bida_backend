import {BaseController} from './base/BaseController';
import Table,{ITable} from '../schema/Table';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import { promises } from 'fs';
import bcryptjs from "bcryptjs";
import mongoose,{Types,Schema} from 'mongoose';

export class TableController extends BaseController implements IBaseController{

    private table:ITable;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.table=new Table();
        this.id="";
        this.request = request;
        
    }


    public setTable(table:ITable):this{
        this.table = table;
        return this;
    }

    public getTable():ITable{
        return this.table;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<ITable>{
        try {
            const params: ITable = this.request.body;
            var table:ITable = await Table.create<ITable>(params);
            return table;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(ITable|null)>{
        try {
            // const params: ITable = this.request.body;
            var tableOld:(ITable|null) = await Table.findOne({_id:this.id});
            var newTable:(ITable|null)= new Table();
            if(tableOld!=null){
                if(tableOld.status=="empty"){
                    newTable = await Table.findOneAndUpdate({_id:this.id},{status:"not-empty"},{new:true});
                }else{
                    newTable = await Table.findOneAndUpdate({_id:this.id},{status:"empty"},{new:true});
                }
            }
            return newTable;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var table = await Table.deleteOne({_id:this.id});
            return table;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(ITable|null)> {
        try {
            var table:(ITable|null) = await Table.findOne({_id:this.id});
            return table;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var tables = await Table.find().populate('table_type_id',["name"],"TableType");
            return tables;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}