import {BaseController} from './base/BaseController';
import {Request} from 'express';
import jwt from "jsonwebtoken";
import {AuthInfo} from "../dto/AuthInfo";
import { GenerateToken } from '../helper/GenerateToken';
import { Auth } from '../helper/Auth';
import { Mailer } from '../helper/Mailer';
import User,{ IUser } from '../schema/User';
import {TOKEN_SECRET} from "../config/index";
import {ResetPassword} from "../dto/ResetPassword";
import bcryptjs from 'bcryptjs';

export class AuthController extends BaseController{

    private authInfo:AuthInfo;

    private request:Request;

    public constructor(request:Request){
        super();
        this.authInfo={
            username:"default",
            password:"default"
        };
        this.request = request;
    }

    public async login():Promise<(null|{token:string,user:IUser})>{
        var auth:Auth = new Auth(this.request);
        var user:(IUser|null) = await auth.execute();

        if(user){
            const token = jwt.sign(
                {
                    _id:user._id,
                    email:user.email,
                    phone:user.phone,
                    role:user.role,
                    firstname:user.firstname,
                    lastname:user.lastname,
                    image : user.image
                },
                TOKEN_SECRET
            );
            return {"token":token,"user":user};
        }else{
            return null;
        }
    }

    private async hashPwd(password:string):Promise<string>{
        try {
            return bcryptjs.hash(password,bcryptjs.genSaltSync(10));
        } catch (error) {
            throw new Error(error);
        }
    }

    public async forgetPassword():Promise<(boolean)>{
        try {
            var email:string = this.request.body.email;
            var user = await User.findOne({email:email});
            if(user){
                var token = new GenerateToken(6).execute();
                var mailer =await new Mailer({"to":email,"html":"please go to web and enter this token to reset password: <strong>"+token+"</strong>","subject":"Bida Reset Password"}).execute();
                await User.findOneAndUpdate({email:email},{"token":token});
                return true;
            }else{
                return false;
            } 
        } catch (error) {
            throw new Error(error);
        }
    }




    public async resetPassword():Promise<IUser|null>{
        try {
            var resetInfo:ResetPassword = this.request.body;

            var newPass = await this.hashPwd(resetInfo.password);

            var user:(IUser|null) = await User.findOneAndUpdate({email:resetInfo.email,token:resetInfo.token},{password:newPass,token:""},{new:true});

            return user;
        } catch (error) {
            throw new Error(error);
        }
    } 

}