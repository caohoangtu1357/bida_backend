import {BaseController} from './base/BaseController';
import ProductAttributeValue,{IProductEavAttributeValue} from '../schema/ProductEavAttributeValue';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import {Schema} from 'mongoose';


export class ProductAttributeValueValueController extends BaseController implements IBaseController{

    private productAttributeValue:IProductEavAttributeValue;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.productAttributeValue=new ProductAttributeValue();
        this.id="";
        this.request = request;
    }


    public setProductAttribute(productAttributeValue:IProductEavAttributeValue):this{
        this.productAttributeValue = productAttributeValue;
        return this;
    }

    public getProductAttribute():IProductEavAttributeValue{
        return this.productAttributeValue;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<IProductEavAttributeValue>{
        try {
            const params: IProductEavAttributeValue = this.request.body;
            this.productAttributeValue = await ProductAttributeValue.create<IProductEavAttributeValue>(params);
            return this.productAttributeValue;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(IProductEavAttributeValue|null)>{
        try {
            const params: IProductEavAttributeValue = this.request.body;
            var product:(IProductEavAttributeValue|null) = await ProductAttributeValue.findOneAndUpdate({_id:this.id},params,{new:true});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var product = await ProductAttributeValue.deleteOne({_id:this.id});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(IProductEavAttributeValue|null)> {
        try {
            var product:(IProductEavAttributeValue|null) = await ProductAttributeValue.findOne({_id:this.id});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var Products = await ProductAttributeValue.find();
            return Products;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}