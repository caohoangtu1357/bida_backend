import {BaseController} from './base/BaseController';
import ProductType,{IProductType} from '../schema/ProductType';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";

export class ProductTypeController extends BaseController implements IBaseController{

    private productType:IProductType;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.productType=new ProductType();
        this.id="";
        this.request = request;
    }


    public setProductType(productType:IProductType):this{
        this.productType = productType;
        return this;
    }

    public getProductType():IProductType{
        return this.productType;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<IProductType>{
        try {
            const params: IProductType = this.request.body;
            var productType:IProductType = await ProductType.create<IProductType>(params);
            return productType;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(IProductType|null)>{
        try {
            const params: IProductType = this.request.body;
            var productType:(IProductType|null) = await ProductType.findOneAndUpdate({_id:this.id},params,{new:true});
            return productType;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var productType = await ProductType.deleteOne({_id:this.id});
            return productType;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(IProductType|null)> {
        try {
            var productType:(IProductType|null) = await ProductType.findOne({_id:this.id});
            return productType;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var ProductTypes = await ProductType.find();
            return ProductTypes;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}