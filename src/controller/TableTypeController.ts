import {BaseController} from './base/BaseController';
import TableType,{ITableType} from '../schema/TableType';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import {Schema} from 'mongoose';

export class TableTypeController extends BaseController implements IBaseController{

    private tableType:ITableType;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.tableType=new TableType();
        this.id="";
        this.request = request;
    }


    public setTableType(tableType:ITableType):this{
        this.tableType = tableType;
        return this;
    }

    public getTableType():ITableType{
        return this.tableType;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<ITableType>{
        try {
            const params: ITableType = this.request.body;
            var tableType:ITableType = await TableType.create<ITableType>(params);
            return tableType;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(ITableType|null)>{
        try {
            const params: ITableType = this.request.body;
            var tableType:(ITableType|null) = await TableType.findOneAndUpdate({_id:this.id},params,{new:true});
            return tableType;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var tableType = await TableType.deleteOne({_id:this.id});
            return tableType;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(ITableType|null)> {
        try {
            var tableType:(ITableType|null) = await TableType.findOne({_id:this.id});
            return tableType;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var tableTypes = await TableType.find();
            return tableTypes;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}