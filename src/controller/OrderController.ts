import {BaseController} from './base/BaseController';
import Order,{IOrder} from '../schema/Order';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import mongoose,{Schema} from 'mongoose';
import {IRequestOrderDetail} from '../dto/RequestOrderDetail';
import { IOrderDetail } from '../schema/OrderDetail';
import OrderDetail from "../schema/OrderDetail";
import { IBill } from '../dto/Bill';
import path from 'path';
import {TableController} from "../controller/TableController";

export class OrderController extends BaseController implements IBaseController{

    private order:IOrder;

    private orderDetails:IRequestOrderDetail;

    private orderDetail:IOrderDetail;

    private bill:IBill;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.order=new Order();

        this.orderDetail=new OrderDetail();

        this.orderDetails={
            order_detail:
            [
                new OrderDetail()
            ]
        };

        this.bill={
            order:new Order(),
            order_detail:[new OrderDetail()]
        };

        this.id="";
        this.request = request;
    }


    public setOrder(order:IOrder):this{
        this.order = order;
        return this;
    }

    public getOrder():IOrder{
        return this.order;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<IBill>{
        try {
            const params: IOrder = this.request.body;
            
            this.orderDetails = this.request.body;

            this.order = await Order.create<IOrder>(params);

            
            let orderDetailCreated:IOrderDetail[]=[];

            for(let i=0 ;i < this.orderDetails.order_detail.length;i++){
                this.orderDetail.requirement = this.orderDetails.order_detail[i].requirement;
                this.orderDetail.product_id = this.orderDetails.order_detail[i].product_id;
                this.orderDetail.order_id = this.order._id;
                orderDetailCreated.push(await OrderDetail.create(this.orderDetail));
            }

            this.bill.order=this.order;
            this.bill.order_detail=orderDetailCreated;

            return this.bill;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(IOrder|null)>{
        try {
            var params: any = this.request.body;
            var order:(IOrder|null) = await Order.findOneAndUpdate({_id:this.id},{status:"da xac nhan"},{new:true});
            var table:TableController = new TableController(this.request);
            if(order!=null){
                table.setId(order.table_id.toString());
                await table.update();
            }
            
            return order;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var order = await Order.deleteOne({_id:this.id});
            var orderDetail = await OrderDetail.deleteMany({order_id:mongoose.Types.ObjectId(this.id)});
            return order;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(IOrder|null)> {
        try {
            var order:(IOrder|null) = await Order.findOne({_id:this.id});
            return order;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var Orders = await Order.find()
            .populate("user_id",["firstname","lastname"],"User")
            .populate("table_id",["name"],"Table");
            return Orders;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}