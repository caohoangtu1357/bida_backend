import {BaseController} from './base/BaseController';
import ProductAttribute,{IProductAttribute} from '../schema/ProductAttribute';
import {Request} from 'express';
import {IBaseController} from "./base/IBaseController";
import {Schema} from 'mongoose';


export class ProductAttributeController extends BaseController implements IBaseController{

    private productAttribute:IProductAttribute;

    private id:string;

    private request:Request;

    public constructor(request:Request){
        super();
        this.productAttribute=new ProductAttribute();
        this.id="";
        this.request = request;
    }


    public setProduct(productAttribute:IProductAttribute):this{
        this.productAttribute = productAttribute;
        return this;
    }

    public getProduct():IProductAttribute{
        return this.productAttribute;
    }

    public getId():string{
        return this.id;
    }

    public setId(id:string):this{
        this.id=id;
        return this;
    }


    public async create():Promise<IProductAttribute>{
        try {
            const params: IProductAttribute = this.request.body;
            this.productAttribute = await ProductAttribute.create<IProductAttribute>(params);
            return this.productAttribute;
        } catch (error) {
            throw new Error(error);
        }
        
    }

    public async update():Promise<(IProductAttribute|null)>{
        try {
            const params: IProductAttribute = this.request.body;
            var product:(IProductAttribute|null) = await ProductAttribute.findOneAndUpdate({_id:this.id},params,{new:true});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async delete(){
        try {
            var product = await ProductAttribute.deleteOne({_id:this.id});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async get():Promise<(IProductAttribute|null)> {
        try {
            var product:(IProductAttribute|null) = await ProductAttribute.findOne({_id:this.id});
            return product;
        } catch (error) {
            throw new Error(error);
        }
    }

    public async gets(){
        try {
            var Products = await ProductAttribute.find();
            return Products;
        } catch (error) {
            throw new Error(error);
        }
        
    }

}