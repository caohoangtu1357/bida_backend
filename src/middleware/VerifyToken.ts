import express,{Request,Response,Application,NextFunction} from 'express';
import { IBaseMiddleware } from './BaseMiddleware/IBaseMiddleware';
import bcrypt from "bcryptjs";
import { AuthInfo } from '../dto/AuthInfo';
import User,{IUser} from '../schema/User';
import jwt from "jsonwebtoken";
import {TOKEN_SECRET} from "../config/index";


export class VerifyToken implements IBaseMiddleware{
    private req:Request;
    private res:Response;
    private next:NextFunction;

    public constructor(req:Request,res:Response,next:NextFunction){
        this.req= req;
        this.res = res;
        this.next = next;
    }

    public execute(){
        var token = this.req.header("auth-token");
        if(!token){
            token = this.req.cookies["auth-cookie"];
        }
        if (!token) {
            return this.res
            .status(401)
            .send({ message: "Bạn không có quyền truy cập, vui lòng đăng nhập" });
        }

        try {
            var verified:(IUser|null) = <IUser|null>jwt.verify(token,TOKEN_SECRET);
            this.req.user = verified;
            return verified;
        } catch (error) {
            return this.res.status(400).send({ message: "Token không hợp lệ" });
        }
    }
}


