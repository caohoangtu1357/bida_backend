import express,{Request,Response,Application,NextFunction} from 'express';
import {BaseRoute} from './base/BaseRoute';
import {RouteInterface} from './base/RouteInterface';
import {AuthController} from '../controller/AuthController';
import { IUser } from '../schema/User';
export class Auth extends BaseRoute{
  
  public constructor(app:Application){
    super(app);
  }

  public routes():void{
    this.login();
    this.forgetPassword();
    this.resetPassword();
    this.isLogged();
  }

  public login():void{
    this.app.route("/login").post(async (req:Request,res:Response,next:NextFunction)=>{
      let authController = new AuthController(req);
      try {
        let auth = await authController.login();
        if(auth?.user){
          res.status(200).header("auth-token",auth?.token).send(auth?.user);
        }else{
          res.status(401).header("auth-token",auth?.token).send({"message":"Invalid credential"});
        }
      } catch (error) {
        console.log(error);
        res.status(500).send(error.message);
      }
    })
  }

  public forgetPassword():void{
    this.app.route("/forget-password").post(async (req:Request,res:Response,next:NextFunction)=>{
      let authController = new AuthController(req);
      try {
        let token = await authController.forgetPassword();
        if(token){
          res.status(200).send({"status":"send token to mail success"});
        }else{
          res.status(500).send({"status":"user not found"});
        }
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public resetPassword():void{
    this.app.route("/reset-password").post(async (req:Request,res:Response,next:NextFunction)=>{
      let authController = new AuthController(req);
      try {
        let user = await authController.resetPassword();
        if(user){
          res.status(200).send({status:"success","user":user,"message":"password changed"});
        }else{
          res.status(500).send({status:"failure","user":user,"message":"invalid token"});
        }
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public isLogged(){
    this.app.route("/is-logged").get(async (req:Request,res:Response,next:NextFunction)=>{

      var user:(IUser|null|undefined|Response) = this.verifyUserLogged(req,res,next);

      try {
        if(user){
          res.status(200).send(user);
        }else{
          res.status(401).json({"message":"User not logged"});  
        }
      } catch (error) {
        res.status(500).json({"message":"Server go down"});  
      }
    })
  }

}