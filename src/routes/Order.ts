import express,{Request,Response,Application,NextFunction} from 'express';
import {BaseRoute} from './base/BaseRoute';
import {RouteInterface} from './base/RouteInterface';
import { OrderController } from '../controller/OrderController';
import {IBill} from "../dto/Bill";

export class Order extends BaseRoute implements RouteInterface{
  
  private prefix:string = "/order";

  public constructor(app:Application){
    super(app);
  }

  public routes():void{
    this.create();
    this.update();
    this.delete();
    this.get();
    this.gets();
  }

  public create():void{
    this.app.route(this.prefix+"/create").post(async (req:Request,res:Response,next:NextFunction)=>{
      var orderController:OrderController = new OrderController(req);
      try {
        let bill:IBill = await orderController.create();
        res.status(200).send(bill);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public update():void{
    this.app.route(this.prefix+"/update/:id").put(async (req:Request,res:Response,next:NextFunction)=>{
      let orderController = new OrderController(req);
      try {
        orderController.setId(req.param("id"));
        let order = await orderController.update();
        res.status(200).send(order);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public get():void{
    this.app.route(this.prefix+"/get/:id").get(async (req:Request,res:Response,next:NextFunction)=>{
      let orderController = new OrderController(req);
      try {
        orderController.setId(req.param("id"));
        let order = await orderController.get();
        res.status(200).send(order);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public gets():void{
    this.app.route(this.prefix+"/gets").get(async (req:Request,res:Response,next:NextFunction)=>{
      let orderController = new OrderController(req);
      try {
        let orders = await orderController.gets();
        res.status(200).send(orders);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public delete():void{
    this.app.route(this.prefix+"/delete/:id").delete(async (req:Request,res:Response,next:NextFunction)=>{
      let orderController = new OrderController(req);
      try {
        orderController.setId(req.param("id"));
        let order = await orderController.delete();
        res.status(200).send(order);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }
}