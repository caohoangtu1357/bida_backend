import express,{Request,Response,Application,NextFunction} from 'express';
import {BaseRoute} from './base/BaseRoute';
import {RouteInterface} from './base/RouteInterface';
import {TableTypeController} from '../controller/TableTypeController';
export class TableType extends BaseRoute implements RouteInterface{
  
  private prefix:string= "/table-type";

  public constructor(app:Application){
    super(app);
  }

  public routes():void{
    this.create();
    this.update();
    this.delete();
    this.get();
    this.gets();
  }

  public create():void{
    this.app.route(this.prefix+"/create").post(async (req:Request,res:Response,next:NextFunction)=>{
      let tableTypeController = new TableTypeController(req);
      try {
        console.log(req.body);
        let user = await tableTypeController.create();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public update():void{
    this.app.route(this.prefix+"/update/:id").put(async (req:Request,res:Response,next:NextFunction)=>{
      let tableTypeController = new TableTypeController(req);
      try {
        tableTypeController.setId(req.param("id"));
        let user = await tableTypeController.update();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public get():void{
    this.app.route(this.prefix+"/get/:id").get(async (req:Request,res:Response,next:NextFunction)=>{
      let tableTypeController = new TableTypeController(req);
      try {
        tableTypeController.setId(req.param("id"));
        let user = await tableTypeController.get();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public gets():void{
    this.app.route(this.prefix+"/gets").get(async (req:Request,res:Response,next:NextFunction)=>{
      let tableTypeController = new TableTypeController(req);
      try {
        let user = await tableTypeController.gets();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public delete():void{
    this.app.route(this.prefix+"/delete/:id").delete(async (req:Request,res:Response,next:NextFunction)=>{
      let tableTypeController = new TableTypeController(req);
      try {
        tableTypeController.setId(req.param("id"));
        let user = await tableTypeController.delete();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }
}