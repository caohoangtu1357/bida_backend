import express,{Request,Response,Application,NextFunction} from 'express';
import {BaseRoute} from './base/BaseRoute';
import {RouteInterface} from './base/RouteInterface';
import {ProductController} from '../controller/ProductController';
export class Product extends BaseRoute implements RouteInterface{
  
  private prefix:string="/product";

  public constructor(app:Application){
    super(app);
  }

  public routes():void{
    this.create();
    this.update();
    this.delete();
    this.get();
    this.gets();
  }

  public create():void{
    this.app.route(this.prefix+"/create").post(async (req:Request,res:Response,next:NextFunction)=>{
      let productController = new ProductController(req);
      try {
        let user = await productController.create();
        res.status(200).send(user);
      } catch (error) {
        console.log(error);
        res.status(500).send(error.message);
      }
    })
  }

  public update():void{
    this.app.route(this.prefix+"/update/:id").put(async (req:Request,res:Response,next:NextFunction)=>{
      let productController = new ProductController(req);
      try {
        productController.setId(req.param("id"));
        let user = await productController.update();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public get():void{
    this.app.route(this.prefix+"/get/:id").get(async (req:Request,res:Response,next:NextFunction)=>{
      let productController = new ProductController(req);
      try {
        productController.setId(req.param("id"));
        let user = await productController.get();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public gets():void{
    this.app.route(this.prefix+"/gets").get(async (req:Request,res:Response,next:NextFunction)=>{
      let productController = new ProductController(req);
      try {
        let user = await productController.gets();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }

  public delete():void{
    this.app.route(this.prefix+"/delete/:id").delete(async (req:Request,res:Response,next:NextFunction)=>{
      let productController = new ProductController(req);
      try {
        productController.setId(req.param("id"));
        let user = await productController.delete();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }
}