import express,{Request,Response,Application,NextFunction,Router} from 'express';
import { BaseRoute } from './base/BaseRoute';
import { User } from './User';
import { Table } from './Table';
import { Auth } from './Auth';
import { Order } from './Order';
import { Product } from './Product';
import {ProductType} from "./ProductType";
import {TableType} from "./TableType";
import { Image } from './Image';
import {ProductAttribute} from './ProductAttribute';
import {productAttributeValue} from "./ProductAttributeValue";
import path from "path";

export class IndexRouter extends BaseRoute{
  

  constructor(app:Application){
    super(app);
    
    this.app.route("/").get(async (req:Request,res:Response,next:NextFunction)=>{
      res.sendFile(path.join(__dirname + '../../../public/index.html'));
    })

    
    this.regisNewRoute(new User(app));
    this.regisNewRoute(new Table(app));
    this.regisNewRoute(new Auth(app));
    this.regisNewRoute(new Order(app));
    this.regisNewRoute(new Product(app));
    this.regisNewRoute(new ProductType(app));
    this.regisNewRoute(new TableType(app));
    this.regisNewRoute(new Image(app));
    this.regisNewRoute(new ProductAttribute(app));
    this.regisNewRoute(new productAttributeValue(app));
    

    //regis new route
  }

  private regisNewRoute(routeClass: BaseRoute):void{
    routeClass.routes();
  }
}