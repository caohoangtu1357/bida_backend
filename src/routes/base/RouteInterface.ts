export interface RouteInterface{
    routes():void;
    create():void;
    update():void;
    delete():void;
    get():void;
    gets():void;
}