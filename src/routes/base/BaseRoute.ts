import express,{Request,Response,Application,NextFunction} from 'express';
import {VerifyToken} from "../../middleware/VerifyToken";
import { IUser } from '../../schema/User';

export abstract class BaseRoute{
    protected app:Application;

    public constructor(app:Application){
      this.app=app;
    }

    public routes(){
        
    }

    protected verifyUserLogged(req:Request,res:Response,next:NextFunction):(IUser|null|undefined|Response){
      var middleWare = new  VerifyToken(req,res,next);
      return middleWare.execute();
    }
}