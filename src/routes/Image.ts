import express,{Request,Response,Application,NextFunction} from 'express';
import {BaseRoute} from './base/BaseRoute';
import {RouteInterface} from './base/RouteInterface';
import {Uploader} from "../helper/FileUploader";

export class Image extends BaseRoute{
  
  private prefix:string = "/image";

  public constructor(app:Application){
    super(app);
  }

  public routes():void{
    this.create();
  }

  public create():void{
    this.app.route(this.prefix+"/upload").post(async (req:Request,res:Response,next:NextFunction)=>{
      try {
        var imageName =await new Uploader("upload/image","image",req,res,next).execute();
        res.status(200).send({uploaded:imageName});
      } catch (error) {
        console.log(error);
        res.status(500).send(error.message);
      }  
    })
  }
}