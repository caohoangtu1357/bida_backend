import express,{Request,Response,Application,NextFunction} from 'express';
import {BaseRoute} from './base/BaseRoute';
import {RouteInterface} from './base/RouteInterface';
import { UserController } from '../controller/UserController';
import { request } from 'http';
import { IUser } from '../schema/User';

export class User extends BaseRoute implements RouteInterface{
  
  private prefix:string = "/user";

  public constructor(app:Application){
    super(app);
    
  }

  public routes():void{
    this.create();
    this.update();
    this.delete();
    this.get();
    this.gets();
  }


  public create(): void{

    this.app.route(this.prefix+"/create").post(async (req:Request,res:Response,next:NextFunction)=>{
      
      let userController = new UserController(req);
      try {
        let user = await userController.create();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })

  }

  public update(): void{

    this.app.route(this.prefix+"/update/:id").put(async (req:Request,res:Response,next:NextFunction)=>{
      

  
      let userController = new UserController(req);
      try {
        userController.setId(req.params.id);
        let user = await userController.update();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })

  }

  public delete(): void{

    this.app.route(this.prefix+"/delete/:id").delete(async (req:Request,res:Response,next:NextFunction)=>{
      
      // this.verifyUserLogged(req,res,next);

      let userController = new UserController(req);
      try {
        userController.setId(req.params.id);
        let user = await userController.delete();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })

  }

  public get(): void{

    this.app.route(this.prefix+"/get/:id").get(async (req:Request,res:Response,next:NextFunction)=>{
      
      // this.verifyUserLogged(req,res,next);

      let userController = new UserController(req);
      try {
        userController.setId(req.params.id);
        let user = await userController.get();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
      
    })
  }

  public gets(): void{

    this.app.route(this.prefix+"/gets").get(async (req:Request,res:Response,next:NextFunction)=>{
      
      // this.verifyUserLogged(req,res,next);

      let userController = new UserController(req);
      try {
        let user = await userController.gets();
        res.status(200).send(user);
      } catch (error) {
        res.status(500).send(error.message);
      }
    })
  }
  
}