import mongoose,{Document, Schema} from 'mongoose';

export interface IUser{
    _id:Schema.Types.ObjectId;
    email?:string;
    firstname:string;
    lastname:string;
    phone:string;
    role:string;
    password:string;
    image:string;
}

const UserSchema: Schema = new Schema({
    email:{
        type:String,
        required:false,
        unique:true
    },
    firstname:{
        type:String,
        required:true,
    },
    lastname:{
        type:String,
        required:true
    },
    phone:{
        type:String,
        required:false
    },
    role:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    token:{
        type:String,
        required:false,
        default:""
    },
    image:{
        type:String,
        required:false,
        default:"default.jpg"

    }
});

export default mongoose.model<IUser & Document>('User',UserSchema);