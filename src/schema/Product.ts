import mongoose,{Document, Schema, Types} from 'mongoose';

export interface IProduct{
    _id:Schema.Types.ObjectId;
    name:string;
    description:string;
    quantity:number;
    product_type_id:Schema.Types.ObjectId;
    image:string;
}

const ProductSchema: Schema = new Schema({
    name:{
        type:String,
        required:true
    },
    description:{
        type:String
    },
    quantity:{
        type:Number
    },
    product_type_id:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:"ProductType"
    },
    image:{
        type:String,
        required:false,
        default:"default.jpg"
    }
});

export default mongoose.model<IProduct & Document>('Product',ProductSchema);