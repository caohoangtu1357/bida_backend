import mongoose,{Document, Schema, Types} from 'mongoose';

export interface IOrderDetail{
    _id:Schema.Types.ObjectId;
    requirement:string;
    product_id: Schema.Types.ObjectId;
    order_id:mongoose.Types.ObjectId;
}

const OrderDetailSchema: Schema = new Schema({
    requirement:{
        type:String
    },

    product_id:{
        type:Schema.Types.ObjectId,
        required:true,
        ref: "Product"
    },

    order_id:{
        type:mongoose.Types.ObjectId,
        required:true,
        ref: "Order"
    }
    
});

export default mongoose.model<IOrderDetail & Document>('OrderDetail',OrderDetailSchema);