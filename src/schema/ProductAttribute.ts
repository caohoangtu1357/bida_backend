import mongoose,{Document, Schema} from 'mongoose';



export interface IProductAttribute {
    _id:Schema.Types.ObjectId;
    name:string;
    type:string;
}

const ProductAttribute: Schema = new Schema({
    name:{
        type:String,
        required:true
    },
    type:{
        type:String,
        required:true
    }
});

export default mongoose.model<IProductAttribute & Document>('ProductAttribute',ProductAttribute);