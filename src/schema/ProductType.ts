import mongoose,{Document, Schema} from 'mongoose';

export interface IProductType{
    _id:Schema.Types.ObjectId;
    name:string;
    image:string;
}

const ProductTypeSchema: Schema = new Schema({
    name:{
        type:String,
        required:true
    },
    image:{
        type:String,
        required:false
    }
});

export default mongoose.model<IProductType & Document>('ProductType',ProductTypeSchema);