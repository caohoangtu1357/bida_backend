import mongoose,{Document, Schema, Types} from 'mongoose';
import { IProduct } from './Product';


export interface IProductEavAttributeValue{
    _id:Schema.Types.ObjectId;
    product_id:Schema.Types.ObjectId;
    product_attribute_id:Schema.Types.ObjectId;
    value:string;
}

const ProductEavAttributeValueSchema: Schema = new Schema({
    product_id:{
        type:Schema.Types.ObjectId,
        required:true,
        ref: "Product"
    },
    product_attribute_id:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:"ProductAttribute"
    },
    value:{
        type:String,
        required:true
    }
});

export default mongoose.model<IProductEavAttributeValue & Document>('ProductEavAttributeValue',ProductEavAttributeValueSchema);