import mongoose,{Document, Schema} from 'mongoose';
import { ITableType } from './TableType';

export interface ITable {
    _id:Schema.Types.ObjectId;
    name:string;
    description:string;
    table_type_id:Schema.Types.ObjectId;
    status?:string;
}

const TableSchema: Schema = new Schema({
    name:{
        type:String,
        required:true
    },
    description:{
        type:String
    },
    table_type_id:{
        type:Schema.Types.ObjectId,
        required:true,
        ref:"TableType"
    },
    status:{
        type:String,
        default:"empty"
    }
});

export default mongoose.model<ITable & Document>('Table',TableSchema);