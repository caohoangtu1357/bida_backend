import mongoose,{Document, Schema} from 'mongoose';

export interface ITableType{
    _id:Schema.Types.ObjectId;
    name:string;
    description:string;
    quantity:number;
    price:number;
}

const TableTypeSchema: Schema = new Schema({
    name:{
        type:String,
        required:true
    },
    description:{
        type:String
    },
    quantity:{
        type:Number,
        default:0
    },
    price:{
        type:Number,
        default:0
    }
});

export default mongoose.model<ITableType & Document>('TableType',TableTypeSchema);