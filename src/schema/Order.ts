import mongoose,{Document, Schema, Types} from 'mongoose';
import {IProduct} from './Product';
import {IUser} from './User';


export interface IOrder{
    _id:mongoose.Types.ObjectId;
    date_order?:Date;
    requirement:string;
    status?:string;
    cost:number;
    user_id:Schema.Types.ObjectId;
    table_id:Schema.Types.ObjectId;
}

const OrderSchema: Schema = new Schema({
    date_order:{
        type:String,
        required:true,
        default: Date.now()
    },
    requirement:{
        type:String
    },
    status:{
        type:String,
        required:false,
        default:"chua xac nhan"
    },
    cost:{
        type:Number,
        required:true
    },
    user_id:{
        type:Schema.Types.ObjectId,
        required:false, //user co the ko phai la thanh vien
        ref: "User"
    },
    table_id:{
        type:Schema.Types.ObjectId,
        required:false,
        ref: "Table"
    }
});

export default mongoose.model<IOrder & Document>('Order',OrderSchema);