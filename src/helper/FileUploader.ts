import multer,{Multer} from "multer";
import {IHelper} from "./Base/IHelper";
import path from "path";
import {Request,Response,NextFunction} from 'express';
const util = require("util");

export class Uploader implements IHelper{


    private destinationPath:string="upload/image";
    private fileName:string;
    private imageName:string="";
    private req:Request;
    private res:Response;
    private next:NextFunction;

    public constructor(destinationPath:string, fileName:string,req:Request,res:Response,next:NextFunction){
        this.destinationPath = destinationPath;
        this.fileName=fileName;
        this.req= req;
        this.res= res;
        this.next = next;
    }

    public async execute():Promise<string>{
        var upload =await util.promisify(this.config().single(this.fileName));
        await upload(this.req, this.res);

        return this.destinationPath+"/"+this.req.file.filename;
    }

    private config():Multer{
        var self = this;
        var storage =  multer.diskStorage({
            destination: async function (req, file, cb) {
               cb(null, path.join(__dirname, "../public/"+self.destinationPath));
            },
            filename: async function (req, file, cb) {
                const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
                var imageName=uniqueSuffix + "-" + file.originalname;
                var reg = new RegExp("[ ]+","g");
                imageName= imageName.replace(reg,"");
                cb(null, imageName);
            },
        });
        return multer({ storage: storage });

    }
}