import nodemailer from "nodemailer";
import {EMAIL_MAILER,PASS_MAILER} from "../config/index";
import {IHelper} from "./Base/IHelper";
import { IMailer } from '../dto/Mailer';


export class Mailer implements IHelper{

    private mailer:IMailer;

    public constructor(mailer:IMailer){
        this.mailer=mailer;
    }

    public async execute():Promise<boolean>{
        var transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: EMAIL_MAILER,
              pass: PASS_MAILER
            }
        });
        try {
            await transporter.sendMail(this.mailer);
            return true;
        } catch (error) {
            throw new Error(error);
        }
    }
}

