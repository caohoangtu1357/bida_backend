import express,{Request,Response,Application,NextFunction} from 'express';
import { IHelper } from './Base/IHelper';

export class GenerateToken implements IHelper{

    private tokenLength:number;

    private makeId(tokenLength:number) {
        var result:string           = '';
        var characters:string       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < tokenLength; i++ ) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
     

    public constructor(tokenLength:number){
        this.tokenLength= tokenLength;
    }

    public setTokenLength(tokenLength:number){
        this.tokenLength = tokenLength;
    }

    public execute(){
        return this.makeId(this.tokenLength);
    }
}
