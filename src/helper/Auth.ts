import express,{Request,Response,Application,NextFunction} from 'express';
import { IHelper } from './Base/IHelper';
import bcrypt from "bcryptjs";
import { AuthInfo } from '../dto/AuthInfo';
import User from '../schema/User';
import { IUser } from '../schema/User';

export class Auth implements IHelper{

    private req:Request;

    public constructor(req:Request){
        this.req= req;
    }

    public async execute():Promise<IUser|null>{

        var authInfo:AuthInfo = <AuthInfo>this.req.body;

        const email = authInfo.username;
        const password = authInfo.password;
        var user = await User.findOne({$or:[{email: email},{phone: email}]});

        if(!user){
            return null;
        }
        
        try {
            var isMatch = await bcrypt.compare(password, user.password);
            if(isMatch){
                return user;
            }else{
                return null;
            }
        } catch (error) {
            throw new Error(error);
        }
    }
}
