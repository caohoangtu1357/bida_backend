import createError from 'http-errors';
import express, {Application,Request,Response,NextFunction,Errback} from 'express';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import {IndexRouter} from './routes/index';
import mongoose from 'mongoose';
import * as dotenv from "dotenv";
import {URL_DB} from "./config/index";
import * as bodyParser from "body-parser";
import cors from "cors";
import { IUser } from './schema/User';


declare global {
  namespace Express {
    interface Request {
      user: (IUser|null);
    }
  }
}

class App {

    private app: express.Application;
    private routes:IndexRouter;
    
    constructor() {
        this.app = express();
        this.config();
        this.connect_db();
        this.routes = new IndexRouter(this.app);  
    }

    public getApp():Application{
        return this.app;
    }

    private config(){

        this.app.use(
          cors({ credentials: true, origin: true, exposedHeaders: ["auth-token"] })
        );

        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: false }));

        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'jade');
        
        this.app.use(logger('dev'));
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(cookieParser());
        console.log(path.join(__dirname, 'public'));
        this.app.use(express.static(path.join(__dirname, '../public')));
    }

    private connect_db(){
        console.log(URL_DB);
        mongoose.connect(
            URL_DB,
            { useNewUrlParser: true, useUnifiedTopology: true },
            err => {
              if (err) {
                console.log("connect fail");
                console.log(err);
              } else {
                console.log("connected to mongodb");
              }
            }
          );
    }
    

}

export default new App().getApp();
